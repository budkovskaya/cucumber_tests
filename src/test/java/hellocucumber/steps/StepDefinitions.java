package hellocucumber.steps;

import hellocucumber.pages.GoogleResultPage;
import hellocucumber.pages.GoogleSearchPage;
import hellocucumber.utils.BrowserFactory;
import hellocucumber.utils.Waiters;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;
import static hellocucumber.pages.GoogleResultPage.URL_AUTOPRACTICE;

public class StepDefinitions {

    private static WebDriver driver;
    protected static GoogleSearchPage googleSearchPage;
    protected static GoogleResultPage googleResultPage;

    @Before
    public static void setUp() {
        try {
            driver = BrowserFactory.CHROME.create();
            driver.manage().window().maximize();
            Waiters.implicitWait(driver, Waiters.TIME_TEN, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Some problems with driver occurred.");
        }
        googleSearchPage = new GoogleSearchPage(getDriver());
        googleResultPage = new GoogleResultPage(getDriver());
    }

    public static WebDriver getDriver() {
        return driver;
    }

    @Given("Google Search page is opened")
    public static void googleSearchPageIsOpened() {
        googleSearchPage.open();
    }

    @When("User searches with text")
    public void userSearchesWithText() {
        googleSearchPage.search("automationpractice");
    }

    @And("User clicks on the search link")
    public void userClicksSearchLink() {
        googleResultPage.openUrlByName();
    }

    @Then("automationpractice website is opened")
    public void automationpracticeWebsiteIsOpened() {
        Assert.assertEquals(URL_AUTOPRACTICE, getDriver().getCurrentUrl());
    }

    @After
    public static void tearDown() {
        driver.quit();
    }

}

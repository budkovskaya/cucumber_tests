$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/CucumberFirtsTest.feature");
formatter.feature({
  "name": "cucumber first test",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "open automationpractice website",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@cucumber"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Google Search page is opened",
  "keyword": "Given "
});
formatter.match({
  "location": "hellocucumber.steps.StepDefinitions.googleSearchPageIsOpened()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User searches with text",
  "keyword": "When "
});
formatter.match({
  "location": "hellocucumber.steps.StepDefinitions.userSearchesWithText()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the search link",
  "keyword": "And "
});
formatter.match({
  "location": "hellocucumber.steps.StepDefinitions.userClicksSearchLink()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "automationpractice website is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "hellocucumber.steps.StepDefinitions.automationpracticeWebsiteIsOpened()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});